const renderEventsData = (data) => {
    let events = {};

    if (!data) {
        return [];
    }

    data.events.forEach((event) => {
        const isStore = (event.event === 'comprou');
        const currentEvent = {};
        let dateAndTime;

        event.custom_data.forEach((data) => {
            var key = data.key;

            currentEvent[key] = data.value;
        });

        if (!events[currentEvent.transaction_id]) {
            dateAndTime = formatDateAndTime(event.timestamp);

            events[currentEvent.transaction_id] = {
                date: dateAndTime.date,
                time: dateAndTime.time,
                timestamp: event.timestamp,
                products: []
            }
        }

        if (isStore) {
            events[currentEvent.transaction_id].revenue = formatMoney(event.revenue);
            events[currentEvent.transaction_id].store = currentEvent.store_name;
            return;
        }

        events[currentEvent.transaction_id].products.push({
            name: currentEvent.product_name,
            price: formatMoney(currentEvent.product_price)
        });
    });

    events = Object.values(events);
    events = events.sort(sortByDesc('timestamp'));

    return events;
};

const formatDateAndTime = (date) => {
    const dateFormatted = {};

    if (!date) {
        return dateFormatted;
    }

    date = new Date(date);

    dateFormatted.date = new Intl.DateTimeFormat('pt-br').format(date);
    dateFormatted.time = new Intl.DateTimeFormat('pt-br', { hour: 'numeric', minute: 'numeric' }).format(date)

    return dateFormatted;
};

const formatMoney = (price) => {
    let priceFormatted;

    if (!price) {
        return '-';
    }

    priceFormatted = new Intl.NumberFormat('pt-br', { style: 'currency', currency: 'BRL' }).format(price)

    return priceFormatted;
};

const sortByDesc = (key) => {
    return (a, b) => {
        if (a[key] < b[key]) {
            return 1;
        }

        if (a[key] > b[key]) {
            return -1;
        }

        return 0;
    };
};

export default {
    renderEventsData
};
