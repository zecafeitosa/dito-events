import axios from 'axios';
import Utils from './Utils';

const API_URL = 'https://storage.googleapis.com/dito-questions/events.json';

export const getEvents = (callback) => {
    axios.get(API_URL)
        .then((response) => {
            callback(Utils.renderEventsData(response.data));
        });
};
