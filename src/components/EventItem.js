import React from 'react';

const EventItem = (props) => (
    <div className="events--item">
        <div className="events--header">
            <ul className="events--header-items">
                <li className="events--header-item">
                    <img src={require('../assets/icons/calendar.svg')} className="events--header-image" alt=""
                         title=""/>
                    {props.event.date}
                </li>
                <li className="events--header-item">
                    <img src={require('../assets/icons/clock.svg')} className="events--header-image" alt="" title=""/>
                    {props.event.time}
                </li>
                <li className="events--header-item">
                    <img src={require('../assets/icons/place.svg')} className="events--header-image" alt="" title=""/>
                    {props.event.store}
                </li>
                <li className="events--header-item">
                    <img src={require('../assets/icons/money.svg')} className="events--header-image" alt="" title=""/>
                    {props.event.revenue}
                </li>
            </ul>
        </div>
        <div className="events--products">
            <table className="table">
                <tbody>
                    <tr>
                        <th>Produto</th>
                        <th width="80">Preço</th>
                    </tr>
                    {props.event.products && props.event.products.map((product) => (
                        <tr key={product.name}>
                            <td>{product.name}</td>
                            <td>{product.price}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    </div>
);

export default EventItem;
