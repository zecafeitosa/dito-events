import React, {Component} from 'react';
import {getEvents} from './services/Api'
import EventItem from './components/EventItem';
import './index.css';

class App extends Component {
    state = {
        events: [],
        loading: true
    };

    componentWillMount() {
        getEvents((events) => {
            this.setState({
                events: events,
                loading: false
            })
        })
    }

    render() {
        return (
            <div className="App">
                <div className="events">
                    {this.state.loading && (
                        <img src={require('./assets/icons/loading.gif')} className="loading" alt="loading" title="loading"/>
                    )}

                    {!this.state.loading && this.state.events && this.state.events.map((event) => (
                        <EventItem key={event.timestamp} event={event}></EventItem>
                    ))}
                </div>
            </div>
        );
    }
}

export default App;
